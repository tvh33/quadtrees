import javax.swing.JFrame;
import java.awt.event.*;
import java.awt.*;

public class Main extends JFrame implements KeyListener {
	private Problem problem;

	public Main() {
		setTitle("Convex hull");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
		setLayout(new BorderLayout());

		problem = new Problem(this);
		add(problem);
		
		addKeyListener(this);

		pack();
		repaint();
	}
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == e.VK_R) {
			problem.reset();
			repaint();
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public static void main(String[] args) {
		Main m = new Main();
	}
}
