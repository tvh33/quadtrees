import javax.swing.JPanel;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class Problem extends JPanel implements MouseListener, MouseMotionListener {

	public static int WIDTH = 512;
	public static int GAP = 32;
	private Main main;
	
	public static Color drawColor = new Color(110.f/255.f, 161.f/255.f, 212.f/255.f);
	public static Color bgBlue = new Color(50.f/255.f, 101.f/255.f, 152.f/255.f);
	public static Color drawThin = new Color(70.f/255.f, 121.f/255.f, 172.f/255.f);

	private ArrayList<Point> points;
	private ArrayList<Point> hullPoints;

	private int[] hullX;
	private int[] hullY;

	private String[] texts = {	"Interactive Quadtree construction - by tvh33",
								" ",
								"Left mouse creates points",
								"Middle mouse toggles neighbor highlighting",
								"Right mouse toggles triangulations",
								"\"C\"-key to toggle color intensities",
								"\"R\"-key to reset"};

	public Problem(Main m) {
		main = m;
		setPreferredSize(new Dimension(WIDTH+2*GAP, WIDTH+2*GAP));

		points = new ArrayList<Point>();
		hullPoints = new ArrayList<Point>();

		hullX = null;
		hullY = null;

		addMouseListener(this);
		addMouseMotionListener(this);
	}


	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;

		// Draw background
		g2d.setColor(Problem.bgBlue);
		g2d.fillRect(0, 0, WIDTH+2*GAP, WIDTH+2*GAP);
		g2d.setColor(Problem.drawThin);
		for(int i=0; i<WIDTH+2*GAP; i+=8) {
			g2d.drawLine(0, i, WIDTH+2*GAP, i);
			g2d.drawLine(i, 0, i, WIDTH+2*GAP);
		}
		g2d.setColor(Problem.bgBlue);
		g2d.fillRect(GAP, GAP, WIDTH, WIDTH);
		g2d.setColor(drawColor);
		g2d.setStroke(new BasicStroke(2));
		g2d.drawRect(GAP, GAP, WIDTH, WIDTH);
		g2d.setStroke(new BasicStroke(1));

		
		// Draw text
		if(points.size() == 0) {
			for(int i=0; i<texts.length; i++) {
				g2d.drawString(texts[i], 2*GAP, GAP+WIDTH/2 + 15*i);
			}
		}
		else {
			fillHull(g2d);
			
			// Draw points
			for(int i=0; i<points.size(); i++) {
				points.get(i).draw(g2d);
			}

			drawHull(g2d);
		}
	}

	public void convertHullToArrays() {
		hullX = new int[hullPoints.size()];
		hullY = new int[hullPoints.size()];
		for(int i=0; i<hullPoints.size(); i++) {
			hullX[i] = hullPoints.get(i).x;
			hullY[i] = hullPoints.get(i).y;
		}
	}

	public void fillHull(Graphics2D g2d) {
		if(hullX == null || hullY == null) return;
		g2d.setColor(drawThin);
		g2d.fillPolygon(hullX, hullY, hullX.length);
	}

	public void drawHull(Graphics2D g2d) {
		if(hullX == null || hullY == null) return;
		g2d.setColor(drawColor);
		g2d.drawPolygon(hullX, hullY, hullX.length);
	}

	public void computeConvexHull() {
		for(Point p : hullPoints) {
			p.marked = false;
		}
		
		hullPoints = convexHull();

		for(Point p : hullPoints) {
			p.marked = true;
		}

		convertHullToArrays();
	}

	public ArrayList<Point> convexHull() {
		Collections.sort(points);
		ArrayList<Point> L_upper = new ArrayList<Point>();
		ArrayList<Point> L_lower = new ArrayList<Point>();

		L_upper.add(points.get(0));
		L_upper.add(points.get(1));

		for(int i=2; i<points.size(); i++) {
			L_upper.add(points.get(i));

			while(L_upper.size() > 2 && !L_upper.get(L_upper.size()-2).toTheRight(
				  L_upper.get(L_upper.size()-3), L_upper.get(L_upper.size()-1))) {
				L_upper.remove(L_upper.size()-2);
			}
		}

		L_lower.add(points.get(points.size()-1));
		L_lower.add(points.get(points.size()-2));

		for(int i=points.size()-1; i>=0; i--) {
			L_lower.add(points.get(i));
			
			while(L_lower.size() > 2 && !L_lower.get(L_lower.size()-2).toTheRight(
				  L_lower.get(L_lower.size()-3), L_lower.get(L_lower.size()-1))) {
				L_lower.remove(L_lower.size()-2);
			}
		}

		for(int i=1; i<L_lower.size()-1; i++) {
			L_upper.add(L_lower.get(i));
		}

		return L_upper;
	}

	public void addPoint(int x, int y) {
		Point newP = new Point(x, y);
		for(Point p : points) {
			if(p.compareTo(newP) == 0)
				return;
		}
		points.add(newP);
	}

	public void reset() {
		hullPoints.clear();
		points.clear();
	}

	public void mousePressed(MouseEvent e) {
		if(e.getButton() == e.BUTTON1) {
			int x = e.getX();
			int y = e.getY();
			if(x > GAP && x < GAP+WIDTH && y > GAP && y < GAP+WIDTH) {
				addPoint(x, y);
			}
		}
		else if(e.getButton() == e.BUTTON3) {
			computeConvexHull();
		}
		main.repaint();
	}

	public void mouseReleased(MouseEvent e) {
	}
	public void mouseDragged(MouseEvent e) {
	}
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseMoved(MouseEvent e) {
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
}
