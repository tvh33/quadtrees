import java.awt.*;

public class Edge {
	public int x1, x2, y1, y2;

	public Edge(int ix1, int iy1, int ix2, int iy2) {
		x1 = ix1;
		x2 = ix2;
		y1 = iy1;
		y2 = iy2;
	}

	public void draw(Graphics2D g2d) {
		g2d.setColor(Problem.drawColor);
		g2d.drawLine(x1, y1, x2, y2);
	}
}
