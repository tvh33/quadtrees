import javax.swing.JPanel;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class Problem extends JPanel implements MouseListener, MouseMotionListener {

	public static int WIDTH = 512;
	public static int GAP = 32;
	private Main main;
	private int startX, startY, endX, endY, curX, curY;

	public static Color drawColor = new Color(110.f/255.f, 161.f/255.f, 212.f/255.f);
	public static Color bgBlue = new Color(50.f/255.f, 101.f/255.f, 152.f/255.f);
	public static Color drawThin = new Color(70.f/255.f, 121.f/255.f, 172.f/255.f);

	private ArrayList<Point> points;

	private QTNode root;

	public static boolean drawNeighbors;
	public static boolean drawEdges;
	public static boolean drawColored;

	private String[] texts = {	"Interactive Quadtree construction - by tvh33",
								" ",
								"Left mouse creates points",
								"Middle mouse toggles neighbor highlighting",
								"Right mouse toggles triangulations",
								"\"C\"-key to toggle color intensities",
								"\"R\"-key to reset"};

	public Problem(Main m) {
		main = m;
		setPreferredSize(new Dimension(WIDTH+2*GAP, WIDTH+2*GAP));

		drawNeighbors = false;
		drawColored = false;
		drawEdges = false;
		startX = startY = endX = endY = curX = curY = -1;

		points = new ArrayList<Point>();

		root = null;

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void makeQuadTree() {
		root = new QTNode(GAP, GAP, WIDTH);
		root.splitNode(points);
	}

	public void balanceQuadTree() {
		LinkedList<QTNode> L = new LinkedList<QTNode>();
		if(root == null) return;

		root.collectLeaves(L);

		while(!L.isEmpty()) {
			QTNode µ = L.poll();
			if(µ.needSplitting()) {
				µ.split(L);
				
				QTNode tmp;
				tmp = µ.northNeighbor();
				if(tmp != null && tmp.sideLength > µ.sideLength)
					tmp.split(L);
				tmp = µ.southNeighbor();
				if(tmp != null && tmp.sideLength > µ.sideLength)
					tmp.split(L);
				tmp = µ.westNeighbor();
				if(tmp != null && tmp.sideLength > µ.sideLength)
					tmp.split(L);
				tmp = µ.eastNeighbor();
				if(tmp != null && tmp.sideLength > µ.sideLength)
					tmp.split(L);
			}
		}
		
		root.computeEdges();
	}

	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D)g;

		// Draw background
		g2d.setColor(Problem.bgBlue);
		g2d.fillRect(0, 0, WIDTH+2*GAP, WIDTH+2*GAP);
		g2d.setColor(Problem.drawThin);
		for(int i=0; i<WIDTH+2*GAP; i+=8) {
			g2d.drawLine(0, i, WIDTH+2*GAP, i);
			g2d.drawLine(i, 0, i, WIDTH+2*GAP);
		}
		g2d.setColor(Problem.bgBlue);
		g2d.fillRect(GAP, GAP, WIDTH, WIDTH);
		g2d.setColor(drawColor);
		g2d.setStroke(new BasicStroke(2));
		g2d.drawRect(GAP, GAP, WIDTH, WIDTH);
		g2d.setStroke(new BasicStroke(1));

		// Draw points
		for(int i=0; i<points.size(); i++) {
			points.get(i).draw(g2d);
		}

		// Draw quads
		if(root != null) {
			root.draw(g2d);
		}

		// Draw text
		else {
			for(int i=0; i<texts.length; i++) {
				g2d.drawString(texts[i], 2*GAP, GAP+WIDTH/2 + 15*i);
			}
		}
	}

	public void addPoint(int x, int y) {
		Point newP = new Point(x, y);
		for(Point p : points) {
			if(p.compareTo(newP) == 0)
				return;
		}
		points.add(newP);
	}

	public void reset() {
		points.clear();
		root = null;
	}

	public void mousePressed(MouseEvent e) {
		if(e.getButton() == e.BUTTON1) {
			int x = e.getX();
			int y = e.getY();
			if(x > GAP && x < GAP+WIDTH && y > GAP && y < GAP+WIDTH) {
				addPoint(x, y);
				makeQuadTree();
				balanceQuadTree();
			}
		}
		else if(e.getButton() == e.BUTTON2) {
			drawNeighbors = !drawNeighbors;
		}
		else if(e.getButton() == e.BUTTON3) {
			drawEdges = !drawEdges;
		}
		main.repaint();
	}

	public void mouseReleased(MouseEvent e) {
	}
	public void mouseDragged(MouseEvent e) {
	}
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseMoved(MouseEvent e) {
		if(root != null) {
			QTNode node = root.queryQuadNode(e.getX(), e.getY());
			Point.selected = node.point;
			QTNode.current = node;
			QTNode.nn = node.northNeighbor();
			QTNode.sn = node.southNeighbor();
			QTNode.wn = node.westNeighbor();
			QTNode.en = node.eastNeighbor();
			main.repaint();
		}
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
}
