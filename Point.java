import java.awt.*;

public class Point implements Comparable<Point> {
	public int x;
	public int y;
	public int hwidth;

	public static Point selected = null;

	public Point(int ix, int iy) {
		x = ix;
		y = iy;

		hwidth = 2;
	}

	public void draw(Graphics2D g2d) {
		g2d.setColor(Problem.drawColor);
		if(Point.selected == this) {
			g2d.drawOval(x-hwidth-3, y-hwidth-3, 2*hwidth+5, 2*hwidth+5);
		}
		g2d.fillRect(x-hwidth, y-hwidth, 2*hwidth, 2*hwidth);
	}

	public int compareTo(Point other) {
		if(x > other.x) return 1;
		if(x < other.x) return -1;
		if(y > other.y) return 1;
		if(y < other.y) return -1;
		return 0;
	}

	public int compareTo(int qx, int qy) {
		if(x > qx) return 1;
		if(x < qx) return -1;
		if(y > qy) return 1;
		if(y < qy) return -1;
		return 0;
	}
}
