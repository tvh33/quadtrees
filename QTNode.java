import java.util.*;
import java.awt.*;

public class QTNode {
	public QTNode parent;
	public QTNode nw;
	public QTNode sw;
	public QTNode ne;
	public QTNode se;

	public boolean leaf;
	public boolean steiner;

	public static QTNode nn = null;
	public static QTNode sn = null;
	public static QTNode wn = null;
	public static QTNode en = null;
	public static QTNode current = null;

	public Point point;

	public int sideLength;
	public int cx, cy;
	public int xmid, ymid;

	public int depth;

	public Color color;

	public ArrayList<Edge> edges;

	public QTNode(int icx, int icy, int length) {
		leaf = true;
		parent = null;
		nw = sw = ne = se = null;
		sideLength = 0;
		cx = icx;
		cy = icy;
		sideLength = length;
		point = null;
		edges = new ArrayList<Edge>();
		xmid = cx + sideLength/2;
		ymid = cy + sideLength/2;
		steiner = false;
		depth = 0;

		color = null;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void splitNode(ArrayList<Point> S) {
		color = computeColor();
		if(S.size() <= 1) {
			if(S.size() == 1) {
				point = S.get(0);
			}
			return;
		}
		else {

			ArrayList<Point> nwSet = new ArrayList<Point>();
			ArrayList<Point> swSet = new ArrayList<Point>();
			ArrayList<Point> neSet = new ArrayList<Point>();
			ArrayList<Point> seSet = new ArrayList<Point>();

			for(Point p : S) {
				if(p.x > xmid && p.y > ymid)
					seSet.add(p);
				if(p.x <= xmid && p.y > ymid)
					swSet.add(p);
				if(p.x <= xmid && p.y <= ymid)
					nwSet.add(p);
				if(p.x > xmid && p.y <= ymid)
					neSet.add(p);
			}

			QTNode nwC = new QTNode(cx, cy, sideLength/2);
			QTNode swC = new QTNode(cx, ymid, sideLength/2);
			QTNode neC = new QTNode(xmid, cy, sideLength/2);
			QTNode seC = new QTNode(xmid, ymid, sideLength/2);

			nw = nwC;
			sw = swC;
			ne = neC;
			se = seC;
			leaf = false;
	
			nw.depth = depth+1;
			sw.depth = depth+1;
			ne.depth = depth+1;
			se.depth = depth+1;

			nw.parent = this;
			sw.parent = this;
			ne.parent = this;
			se.parent = this;
			
			nw.splitNode(nwSet);
			sw.splitNode(swSet);
			ne.splitNode(neSet);
			se.splitNode(seSet);
		}
	}

	public QTNode northNeighbor() {
		if(parent == null) {
			return null;
		}
		if(this == parent.sw) {
			return parent.nw;
		}
		if(this == parent.se) {
			return parent.ne;
		}
		QTNode µ = parent.northNeighbor();
		if(µ == null || µ.isLeaf()) {
			return µ;
		}
		else {
			if(this == parent.nw) {
				return µ.sw;
			}
			else {
				return µ.se;
			}
		}
	}

	public QTNode southNeighbor() {
		if(parent == null) {
			return null;
		}
		if(this == parent.nw) {
			return parent.sw;
		}
		if(this == parent.ne) {
			return parent.se;
		}
		QTNode µ = parent.southNeighbor();
		if(µ == null || µ.isLeaf()) {
			return µ;
		}
		else {
			if(this == parent.sw) {
				return µ.nw;
			}
			else {
				return µ.ne;
			}
		}
	}

	public QTNode westNeighbor() {
		if(parent == null) {
			return null;
		}
		if(this == parent.ne) {
			return parent.nw;
		}
		if(this == parent.se) {
			return parent.sw;
		}
		QTNode µ = parent.westNeighbor();
		if(µ == null || µ.isLeaf()) {
			return µ;
		}
		else {
			if(this == parent.nw) {
				return µ.ne;
			}
			else {
				return µ.se;
			}
		}
	}

	public QTNode eastNeighbor() {
		if(parent == null) {
			return null;
		}
		if(this == parent.nw) {
			return parent.ne;
		}
		if(this == parent.sw) {
			return parent.se;
		}
		QTNode µ = parent.eastNeighbor();
		if(µ == null || µ.isLeaf()) {
			return µ;
		}
		else {
			if(this == parent.ne) {
				return µ.nw;
			}
			else {
				return µ.sw;
			}
		}
	}

	public boolean needSplitting() {
		QTNode tmp;
		tmp = northNeighbor();
		if(tmp != null && !tmp.isLeaf() && (!tmp.se.isLeaf() || !tmp.sw.isLeaf())) {
			return true;	
		}
		tmp = southNeighbor();
		if(tmp != null && !tmp.isLeaf() && (!tmp.ne.isLeaf() || !tmp.nw.isLeaf())) {
			return true;
		}
		tmp = westNeighbor();
		if(tmp != null && !tmp.isLeaf() && (!tmp.ne.isLeaf() || !tmp.se.isLeaf())) {
			return true;
		}
		tmp = eastNeighbor();
		if(tmp != null && !tmp.isLeaf() && (!tmp.nw.isLeaf() || !tmp.sw.isLeaf())) {
			return true;
		}
		return false;
	}

	public void split(LinkedList<QTNode> L) {
		QTNode nwC = new QTNode(cx, cy, sideLength/2);
		QTNode swC = new QTNode(cx, ymid, sideLength/2);
		QTNode neC = new QTNode(xmid, cy, sideLength/2);
		QTNode seC = new QTNode(xmid, ymid, sideLength/2);

		nw = nwC;
		sw = swC;
		ne = neC;
		se = seC;
		leaf = false;

		nw.depth = depth+1;
		sw.depth = depth+1;
		ne.depth = depth+1;
		se.depth = depth+1;
		
		nw.color = computeColor();
		sw.color = computeColor();
		ne.color = computeColor();
		se.color = computeColor();

		nw.parent = this;
		sw.parent = this;
		ne.parent = this;
		se.parent = this;

		if(point != null) {
			if(point.x > xmid && point.y > ymid)
				se.point = point;
			else if(point.x <= xmid && point.y > ymid)
				sw.point = point;
			else if(point.x <= xmid && point.y <= ymid)
				nw.point = point;
			else if(point.x > xmid && point.y <= ymid)
				ne.point = point;

			point = null;
		}

		L.add(nw);
		L.add(sw);
		L.add(ne);
		L.add(se);
	}

	public void collectLeaves(LinkedList<QTNode> leaves) {
		if(leaf) {
			leaves.add(this);
		}
		else {
			nw.collectLeaves(leaves);
			ne.collectLeaves(leaves);
			sw.collectLeaves(leaves);
			se.collectLeaves(leaves);
		}
	}

	public QTNode queryQuadNode(int qx, int qy) {
		if(leaf) {
			return this;
		}
		if(qx > xmid && qy > ymid)
			return se.queryQuadNode(qx, qy);
		if(qx <= xmid && qy > ymid)
			return sw.queryQuadNode(qx, qy);
		if(qx <= xmid && qy <= ymid)
			return nw.queryQuadNode(qx, qy);
		if(qx > xmid && qy <= ymid)
			return ne.queryQuadNode(qx, qy);
		return null;
	}

	public void addSteiner() {
		edges.add(new Edge(xmid, ymid, cx, cy));
		edges.add(new Edge(xmid, ymid, cx+sideLength, cy));
		edges.add(new Edge(xmid, ymid, cx, cy+sideLength));
		edges.add(new Edge(xmid, ymid, cx+sideLength, cy+sideLength));
		steiner = true;
	}

	public void computeEdges() {
		if(leaf) {
			QTNode tmp;
			tmp = northNeighbor();
			if(tmp != null && !tmp.isLeaf()) {
				if(!steiner) addSteiner();
				edges.add(new Edge(xmid, ymid, cx+sideLength/2, cy));
			}
			tmp = southNeighbor();
			if(tmp != null && !tmp.isLeaf()) {
				if(!steiner) addSteiner();
				edges.add(new Edge(xmid, ymid, cx+sideLength/2, cy+sideLength));
			}
			tmp = westNeighbor();
			if(tmp != null && !tmp.isLeaf()) {
				if(!steiner) addSteiner();
				edges.add(new Edge(xmid, ymid, cx, cy+sideLength/2));
			}
			tmp = eastNeighbor();
			if(tmp != null && !tmp.isLeaf()) {
				if(!steiner) addSteiner();
				edges.add(new Edge(xmid, ymid, cx+sideLength, cy+sideLength/2));
			}
			if(!steiner) {
				edges.add(new Edge(cx, cy, cx+sideLength, cy+sideLength));
			}
		}
		else {
			nw.computeEdges();
			sw.computeEdges();
			ne.computeEdges();
			se.computeEdges();
		}
	}

	public void drawConnection(Graphics2D g2d, QTNode n) {
		if(n != null) {
			g2d.drawLine(cx+sideLength/2, cy+sideLength/2,
						 n.cx+n.sideLength/2, n.cy+n.sideLength/2);
		}
	}

	public Color computeColor() {
		float intensity = ((float)depth)/10.f;
		intensity = 1.f - intensity;
		return new Color(intensity, intensity, intensity);
	}

	public void draw(Graphics2D g2d) {
		if(!isLeaf()) {
			nw.draw(g2d);
			sw.draw(g2d);
			ne.draw(g2d);
			se.draw(g2d);
			return;
		}
		if(Problem.drawNeighbors && 
			(this==QTNode.nn || this==QTNode.sn || 
			 this==QTNode.wn || this==QTNode.en)) {
			g2d.setColor(Color.red);
			g2d.setStroke(new BasicStroke(2));
			g2d.drawRect(cx, cy, sideLength, sideLength);
			g2d.setStroke(new BasicStroke(1));
		}
		else {
			if(Problem.drawColored) {
				g2d.setColor(color);
				g2d.fillRect(cx, cy, sideLength, sideLength);
			}
			g2d.setColor(Problem.drawColor);
			g2d.drawRect(cx, cy, sideLength, sideLength);
		}
		if(!Problem.drawColored && Problem.drawEdges) {
			for(Edge e : edges) {
				e.draw(g2d);
			}
		}
		if(Problem.drawNeighbors && this == current) {
			g2d.setColor(Color.red);
			drawConnection(g2d, nn);
			drawConnection(g2d, sn);
			drawConnection(g2d, wn);
			drawConnection(g2d, en);
		}
	}
}
